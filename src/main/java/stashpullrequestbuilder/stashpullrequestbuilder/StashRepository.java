package stashpullrequestbuilder.stashpullrequestbuilder;

import com.google.common.collect.Iterables;
import hudson.model.AbstractBuild;
import hudson.model.Result;

import jenkins.model.Jenkins;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.lang.StringUtils;
import stashpullrequestbuilder.stashpullrequestbuilder.stash.*;
import stashpullrequestbuilder.stashpullrequestbuilder.stash.StashPrRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nathan McCarthy
 */
public class StashRepository {
    private static final Logger logger = Logger.getLogger(StashRepository.class.getName());
    public static final String BUILD_START_MARKER = "[BuildStarted:* **%s** %s into %s";
    public static final String BUILD_FINISH_MARKER = "*BuildFinished:* **%s** %s into %s";

    public static final String BUILD_FINISH_REGEX = ".*BuildFinished.*";

    public static final String BUILD_FINISH_SENTENCE = BUILD_FINISH_MARKER + " \n\n **[%s](%s)** - Build #%d which took %s";

    public static final String BUILD_SUCCESS_COMMENT =  "✓ BUILD SUCCESS";
    public static final String BUILD_FAILURE_COMMENT = "✕ BUILD FAILURE";
    public static final String BUILD_UNSTABLE_COMMENT = "⁉ BUILD UNSTABLE";
    public static final String BUILD_ABORTED_COMMENT = "‼ BUILD ABORTED";
    public static final String BUILD_NOTBUILT_COMMENT = "✕ BUILD INCOMPLETE";

    private String projectPath;
    private StashPullRequestsBuilder builder;
    private StashBuildTrigger trigger;
    private StashApiClient client;
    private final StashPullRequestBuildHistory buildHistory;

    public StashRepository(String projectPath, StashPullRequestsBuilder builder,
            StashPullRequestBuildHistory buildHistory) {
        this.projectPath = projectPath;
        this.builder = builder;
        this.buildHistory = buildHistory;
    }

    public void init() {
        trigger = this.builder.getTrigger();
        client = new StashApiClient(
                trigger.getStashHost(),
                trigger.getUsername(),
                trigger.getPassword(),
                trigger.getProjectCode(),
                trigger.getRepositoryName(),
                trigger.isIgnoreSsl());
        this.buildHistory.setClient(client);
    }

    public Collection<StashPr> getTargetPullRequests() {
        logger.info("Fetch PullRequests.");
        List<StashPr> pullRequests = client.getPullRequests();
        List<StashPr> targetPullRequests = new ArrayList<StashPr>();
        for(StashPr pullRequest : pullRequests) {
            if (shouldBuildTarget(pullRequest)) {
                targetPullRequests.add(pullRequest);
            }
        }
        return targetPullRequests;
    }

    public void addFutureBuildTasks(Collection<StashPr> pullRequests) {
        for(StashPr pullRequest : pullRequests) {
                if (trigger.getDeletePreviousBuildFinishComments()) {
                    deletePreviousBuildFinishedComments(pullRequest);
                }
            StashCause cause = new StashCause(trigger.getStashHost(), pullRequest, client);
            this.builder.getTrigger().startJob(cause);
        }
    }

    public void deletePullRequestComment(String pullRequestId, String commentId) {
        this.client.deletePullRequestComment(pullRequestId, commentId);
    }

    private String getMessageForBuildResult(Result result) {
        String message = BUILD_FAILURE_COMMENT;
        if (result == Result.SUCCESS) {
            message = BUILD_SUCCESS_COMMENT;
        }
        if (result == Result.UNSTABLE) {
            message = BUILD_UNSTABLE_COMMENT;
        }
        if (result == Result.ABORTED) {
            message = BUILD_ABORTED_COMMENT;
        }
        if (result == Result.NOT_BUILT) {
            message = BUILD_NOTBUILT_COMMENT;
        }
        return message;
    }
    
    public void postFinishedComment(String pullRequestId, String sourceCommit, String destinationCommit, Result buildResult, String buildUrl, int buildNumber, String additionalComment, String duration) {
        String message = getMessageForBuildResult(buildResult);
        String comment = String.format(BUILD_FINISH_SENTENCE,
                builder.getProject().getDisplayName(),
                StringUtils.substring(sourceCommit, 0, 11),
                StringUtils.substring(destinationCommit, 0, 11),
                message,
                buildUrl,
                buildNumber,
                duration);

        comment = comment.concat(additionalComment);
        this.client.postPullRequestComment(pullRequestId, comment);
    }

    private Boolean isPullRequestMergeable(@Nonnull StashPr pullRequest) {
        if (trigger.isCheckMergeable() || trigger.isCheckNotConflicted()) {
            StashPrMergeableResponse mergeable = client.getPullRequestMergeStatus(pullRequest.getId());
            if (trigger.isCheckMergeable())
                return  mergeable.getCanMerge();
            if (trigger.isCheckNotConflicted())
                return !mergeable.getConflicted();
        }
        return true;
    }

    private void deletePreviousBuildFinishedComments(@Nonnull StashPr pullRequest) {

        StashPrRepository destination = pullRequest.getToRef();
        String owner = destination.getRepository().getProjectName();
        String repositoryName = destination.getRepository().getRepositoryName();
        String id = pullRequest.getId();

        List<StashPrComment> comments = client.getPullRequestComments(owner, repositoryName, id);
        if (comments == null) {
            return;
        }

        Collections.sort(comments);
        Collections.reverse(comments);
        for (StashPrComment comment : comments) {
            String content = comment.getText();
            if (StringUtils.isEmpty(content)) {
                continue;
            }

            Matcher finishMatcher = Pattern.compile(BUILD_FINISH_REGEX, Pattern.CASE_INSENSITIVE).matcher(content);
            if (finishMatcher.find()) {
                logger.info("Found previous build finished comment: " + content);
                deletePullRequestComment(pullRequest.getId(), comment.getCommentId().toString());
            }
        }
    }

    private boolean shouldBuildTarget(@Nonnull StashPr pullRequest) {
        if (!StringUtils.equals(pullRequest.getState(), "OPEN")) {
            return false;
        }

        if (isSkipBuild(pullRequest.getTitle())) {
            logger.info("Skipping PR: " + pullRequest.getId() + " as title contained skip phrase");
            return false;
        }

        if (!isForTargetBranch(pullRequest)) {
            logger.info("Skipping PR: " + pullRequest.getId() + " as targeting branch: " + pullRequest.getToRef().getBranch().getName());
            return false;
        }

        if (!isPullRequestMergeable(pullRequest)) {
            logger.info("Skipping PR: " + pullRequest.getId() + " as cannot be merged");
            return false;
        }

        Integer triggerCommentId = -1;
        String sourceCommit = pullRequest.getFromRef().getLatestCommit();

        StashPrRepository destination = pullRequest.getToRef();
        String owner = destination.getRepository().getProjectName();
        String repositoryName = destination.getRepository().getRepositoryName();

        if (trigger.isOnlyBuildOnComment()) {
            String id = pullRequest.getId();
            List<StashPrComment> comments = client.getPullRequestComments(owner, repositoryName, id);
            Collections.sort(comments);
            Collections.reverse(comments);
            for (StashPrComment comment : comments) {
                String content = comment.getText();
                if (StringUtils.isEmpty(content)) {
                    continue;
                }

                if (isPhrasesContain(content, this.trigger.getCiBuildPhrases())) {
                    logger.info("Comment contains CI build phrase: " + content);
                    triggerCommentId = comment.getCommentId();
                    break;
                }
            }

            if(triggerCommentId != -1 && !buildHistory.commentWasBuilt(triggerCommentId)) {
                logger.info("Comment wasn't built: " + triggerCommentId);
                buildHistory.saveCommentInvocation(triggerCommentId);
                return true;
            }

            return false;
        }

        if(!buildHistory.commitWasBuilt(sourceCommit, destination.getBranch().getName(), trigger.getBuilder().getProject())) {
            logger.info("Commit wasn't built: " + sourceCommit + " on branch " + destination.getBranch().getName());
            buildHistory.saveInvocation(sourceCommit, destination.getBranch().getName());
            return true;
        }

        return false;
    }

    private boolean isForTargetBranch(@Nonnull StashPr pullRequest) {
        String targetBranchesToBuild = this.trigger.getTargetBranchesToBuild();
        if (targetBranchesToBuild !=null && !"".equals(targetBranchesToBuild)) {
            String[] branches = targetBranchesToBuild.split(",");
            for(String branch : branches) {
                if (pullRequest.getToRef().getBranch().getName().matches(branch.trim())) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    private boolean isSkipBuild(@Nullable String pullRequestTitle) {
        String skipPhrases = this.trigger.getCiSkipPhrases();
        if (skipPhrases != null && !"".equals(skipPhrases)) {
            String[] phrases = skipPhrases.split(",");
            for(String phrase : phrases) {
                if (isPhrasesContain(pullRequestTitle, phrase)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isPhrasesContain(@Nullable String text, @Nonnull String phrase) {
        return text != null && text.toLowerCase().contains(phrase.trim().toLowerCase());
    }

    /**
     * Posts commit build status
     * @param build build that was completed
     * @param cause cause that created this job execution
     */
    public void postCommitBuildStatus(String state, AbstractBuild build, StashCause cause) {
        List<NameValuePair> params = new ArrayList<NameValuePair>(5);
        params.add(new NameValuePair("state", state));
        params.add(new NameValuePair("key", String.format("%s-%s-%s-%s-%s",
                                                cause.getDestinationRepositoryOwner(),
                                                cause.getDestinationRepositoryName(),
                                                cause.getPullRequestId(),
                                                cause.getTargetBranch(),
                                                build.getProject().getName())));
        params.add(new NameValuePair("name", "Jenkins CI build " + build.getDisplayName()));
        params.add(new NameValuePair("url", Jenkins.getInstance().getRootUrl() + build.getUrl()));
        params.add(new NameValuePair("description", build.getProject().getDisplayName()));
        client.postCommitBuilt(cause.getSourceCommitHash(), params);
    }

    /**
     * Posts approve/need work if we're in reviewers for this PR
     * @param result result of build that has finished, null in case we want to unapprove
     * @param cause cause that created this build
     */
    public void postApproveStatus(@Nullable Result result, @Nonnull StashCause cause) {
        String username = trigger.getUsername();
        StashPr pullRequest = cause.getRawData();

        if (result == Result.ABORTED || result == Result.NOT_BUILT) {
            return; // build didn't finish successfully, don't do anything
        }

        if (CollectionUtils.isEmpty(pullRequest.getReviewers())) {
            logger.log(Level.FINE, "No reviewers for PR#{0} found, skipping approve", cause.getPullRequestId());
            return;
        }

        for (StashPrReviewer reviewer : pullRequest.getReviewers()) {
            if (reviewer.getUser() == null) {
                continue; // no user for this reviewer, skip...
            }

            if (StringUtils.isEmpty(reviewer.getUser().getSlug())) {
                continue; // slug is empty, we can't use this
            }

            if (StringUtils.equals(reviewer.getUser().getName(), username)) {
                client.postApproveStatus(pullRequest, reviewer, result);
                return;
            }
        }
    }
}
