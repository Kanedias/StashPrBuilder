package stashpullrequestbuilder.stashpullrequestbuilder;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hudson.model.Cause;
import stashpullrequestbuilder.stashpullrequestbuilder.stash.StashApiClient;
import stashpullrequestbuilder.stashpullrequestbuilder.stash.StashPrComment;
import stashpullrequestbuilder.stashpullrequestbuilder.stash.StashPr;
import stashpullrequestbuilder.stashpullrequestbuilder.stash.StashPrRepository;

/**
 * Created by Nathan McCarthy
 */
public class StashCause extends Cause {

    public static final String ADDITIONAL_PARAMETER_REGEX = "^p:(([A-Za-z_0-9])+)=(.*)";
    public static final Pattern ADDITIONAL_PARAMETER_REGEX_PATTERN = Pattern.compile(ADDITIONAL_PARAMETER_REGEX);

    private final String sourceBranch;
    private final String targetBranch;
    private final String sourceRepositoryOwner;
    private final String sourceRepositoryName;
    private final String pullRequestId;
    private final String destinationRepositoryOwner;
    private final String destinationRepositoryName;
    private final String pullRequestTitle;
    private final String sourceCommitHash;
    private final String destinationCommitHash;
    private final String stashHost;
    private final Map<String,String> additionalParameters;
    private final StashPr rawData;

    public StashCause(String stashHost, StashPr pullRequest, StashApiClient client) {
        this.sourceBranch = pullRequest.getFromRef().getBranch().getName();
        this.targetBranch = pullRequest.getToRef().getBranch().getName();
        this.sourceRepositoryOwner = pullRequest.getFromRef().getRepository().getProjectName();
        this.sourceRepositoryName = pullRequest.getFromRef().getRepository().getRepositoryName();
        this.pullRequestId = pullRequest.getId();
        this.destinationRepositoryOwner = pullRequest.getToRef().getRepository().getProjectName();
        this.destinationRepositoryName = pullRequest.getToRef().getRepository().getRepositoryName();
        this.pullRequestTitle = pullRequest.getTitle();
        this.sourceCommitHash = pullRequest.getFromRef().getLatestCommit();
        this.destinationCommitHash = pullRequest.getToRef().getLatestCommit();
        this.rawData = pullRequest;
        this.additionalParameters = getAdditionalParameters(client, pullRequest);
        this.stashHost = stashHost.replaceAll("/$", "");
    }

    public String getSourceBranch() {
        return sourceBranch;
    }
    public String getTargetBranch() {
        return targetBranch;
    }

    public String getSourceRepositoryOwner() {
        return sourceRepositoryOwner;
    }

    public String getSourceRepositoryName() {
        return sourceRepositoryName;
    }

    public String getPullRequestId() {
        return pullRequestId;
    }


    public String getDestinationRepositoryOwner() {
        return destinationRepositoryOwner;
    }

    public String getDestinationRepositoryName() {
        return destinationRepositoryName;
    }

    public String getPullRequestTitle() {
        return pullRequestTitle;
    }

    public String getSourceCommitHash() { return sourceCommitHash; }

    public String getDestinationCommitHash() { return destinationCommitHash; }

    public Map<String,String> getAdditionalParameters() { return additionalParameters; }

    public StashPr getRawData() {
        return rawData;
    }

    public Map<String, String> getAdditionalParameters(StashApiClient client, StashPr pullRequest){
        StashPrRepository destination = pullRequest.getToRef();
        String owner = destination.getRepository().getProjectName();
        String repositoryName = destination.getRepository().getRepositoryName();

        String id = pullRequest.getId();
        List<StashPrComment> comments = client.getPullRequestComments(owner, repositoryName, id);
        if (comments != null) {
            Collections.sort(comments);
//          Collections.reverse(comments);

            Map<String, String> result = new TreeMap<String, String>();

            for (StashPrComment comment : comments) {
                String content = comment.getText();
                if (content == null || content.isEmpty()) {
                    continue;
                }

                Map<String,String> parameters = getParametersFromContent(content);
                for(String key : parameters.keySet()){
                    result.put(key, parameters.get(key));
                }
            }
            return result;
        }
        return null;
    }

    public static Map<String, String> getParametersFromContent(String content){
        Map<String, String> result = new TreeMap<String, String>();
        String lines[] = content.split("\\r?\\n|\\r");
        for(String line : lines){
            AbstractMap.SimpleEntry<String,String> parameter = getParameter(line);
            if(parameter != null){
                result.put(parameter.getKey(), parameter.getValue());
            }
        }

        return result;
    }

    public static AbstractMap.SimpleEntry<String,String> getParameter(String content){
        if(content.isEmpty()){
            return null;
        }
        Matcher parameterMatcher = ADDITIONAL_PARAMETER_REGEX_PATTERN.matcher(content);
        if(parameterMatcher.find(0)){
            String parameterName = parameterMatcher.group(1);
            String parameterValue = parameterMatcher.group(3);
            return new AbstractMap.SimpleEntry<String,String>(parameterName, parameterValue);
        }
        return null;
    }

    @Override
    public String getShortDescription() {
        return "<a href=\"" + stashHost + "/projects/" + this.getDestinationRepositoryOwner() + "/repos/" +
                this.getDestinationRepositoryName() + "/pull-requests/" + this.getPullRequestId() +
                "\" >PR #" + this.getPullRequestId() + " " + this.getPullRequestTitle() + " </a>";
    }
}

