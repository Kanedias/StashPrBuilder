package stashpullrequestbuilder.stashpullrequestbuilder;

import org.jsoup.helper.Validate;
import stashpullrequestbuilder.stashpullrequestbuilder.stash.StashPr;
import hudson.model.AbstractProject;
import java.util.Collection;
import java.util.logging.Logger;

/**
 * Created by Nathan McCarthy
 */
public class StashPullRequestsBuilder {
    private static final Logger logger = Logger.getLogger(StashPullRequestsBuilder.class.getName());
    private final StashPullRequestBuildHistory buildHistory = new StashPullRequestBuildHistory();
    private AbstractProject<?, ?> project;
    private StashBuildTrigger trigger;
    private StashBuilds builds;
    private StashRepository repository;

    public StashPullRequestsBuilder(StashBuildTrigger sbt, AbstractProject<?, ?> project) {
        Validate.notNull(sbt, "Trigger must not be null");
        Validate.notNull(project, "Project must not be null");
        this.trigger = sbt;
        this.project = project;
    }

    public void stop() {
    }

    public void run() {
        logger.info("Check start, project: " + project.getDisplayName());
        this.repository.init();
        Collection<StashPr> targetPullRequests = this.repository.getTargetPullRequests();
        this.repository.addFutureBuildTasks(targetPullRequests);
    }

    public void setup() {
        this.repository = new StashRepository(this.trigger.getProjectPath(), this, buildHistory);
        this.builds = new StashBuilds(this.trigger, this.repository);
    }

    public AbstractProject<?, ?> getProject() {
        return this.project;
    }

    public StashBuildTrigger getTrigger() {
        return this.trigger;
    }

    public StashBuilds getBuilds() {
        return this.builds;
    }
}
