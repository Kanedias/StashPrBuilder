package stashpullrequestbuilder.stashpullrequestbuilder;

import hudson.model.AbstractProject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import stashpullrequestbuilder.stashpullrequestbuilder.stash.StashApiClient;
import stashpullrequestbuilder.stashpullrequestbuilder.stash.StashCommitBuildStatusResponse;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author blaffoy
 */
public class StashPullRequestBuildHistory implements Serializable {
    private static final Logger logger = Logger.getLogger(StashPullRequestBuildHistory.class.getName());

    private final HashSet<Invocation> commitTriggerHistory;
    private final HashSet<Integer> commentTriggerHistory;
    private StashApiClient client;

    private class Invocation {
        public final String sourceSha;
        public final String target;

        public Invocation(String sourceSha, String target) {
            this.sourceSha = sourceSha;
            this.target = target;
        }

        @Override
        public String toString() {
            return "source commit: \"" + sourceSha + ", target branch: \"" + target + "\"";
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Invocation))
                return false;
            if (obj == this)
                return true;

            Invocation rhs = (Invocation) obj;
            return sourceSha.equals(rhs.sourceSha) && target.equals(rhs.target);
        }

        @Override
        public int hashCode() {
            int hash = 23;
            int mult = 31;
            hash = this.sourceSha.hashCode() + hash * mult;
            hash = this.target.hashCode() + hash * mult;
            return hash;
        }
    }

    public StashPullRequestBuildHistory() {
        logger.log(Level.INFO, "Setting up new Build History");
        this.commitTriggerHistory = new HashSet<>();
        this.commentTriggerHistory = new HashSet<>();
    }

    public void saveInvocation(String sourceSha, String targetBranch) {
        Invocation inv = new Invocation(sourceSha, targetBranch);
        if (checkInvoked(inv)) {
            logger.log(Level.SEVERE, "Invocation trigger history already contains {0}", inv);
        } else {
            commitTriggerHistory.add(inv);
        }
    }

    private boolean checkInvoked(Invocation inv) {
        return commitTriggerHistory.contains(inv);
    }

    public boolean commitWasBuilt(String sourceSha, String destinationBranch, AbstractProject<?, ?> project) {
        if(commitTriggerHistory.contains(new Invocation(sourceSha, destinationBranch)))
            return true;

        // deep check (in case we rebooted Jenkins)
        try {
            StashCommitBuildStatusResponse response = client.getCommitBuildStatus(sourceSha);
            if (response.getValues() == null)
                return false;

            for (StashCommitBuildStatusResponse.BuildStatus status : response.getValues()) {
                boolean sameTarget = StringUtils.endsWith(status.getKey(), destinationBranch + "-" + project.getName());
                boolean success = status.getState().equals("SUCCESSFUL");
                if (sameTarget && success) {
                    logger.log(Level.INFO, "Commit was already built: " + status.getKey());
                    return true;
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error trying to parse commit build status response", e);
            return false;
        }
        return false;
    }

    public void saveCommentInvocation(Integer commentId) {
        if (commentWasBuilt(commentId)) {
            logger.log(Level.SEVERE, "Comment trigger history already contains {0}", commentId);
        } else {
            commentTriggerHistory.add(commentId);
        }
    }

    public boolean commentWasBuilt(Integer commentId) {
        return commentTriggerHistory.contains(commentId);
    }

    public void setClient(StashApiClient client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}