package stashpullrequestbuilder.stashpullrequestbuilder.stash;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by Nathan on 20/03/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StashPrActivity implements Comparable<StashPrActivity> {
    private StashPrComment comment;

    public StashPrComment getComment() {
        return comment;
    }

    public void setComment(StashPrComment comment) {
        this.comment = comment;
    }

    public int compareTo(StashPrActivity target) {
        if (this.comment == null || target.getComment() == null) {
            return -1;
        }
        if (this.comment.getCommentId() > target.getComment().getCommentId()) {
            return 1;
        } else if (this.comment.getCommentId().equals(target.getComment().getCommentId())) {
            return 0;
        } else {
            return -1;
        }
    }
}
