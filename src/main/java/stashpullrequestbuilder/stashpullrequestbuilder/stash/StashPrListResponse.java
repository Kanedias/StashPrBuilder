package stashpullrequestbuilder.stashpullrequestbuilder.stash;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created by Nathan McCarthy
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StashPrListResponse {

    private List<StashPr> prValues;
    private int size;
    private boolean isLastPage;
    private int nextPageStart;

    @JsonProperty("values")
    public List<StashPr> getPrValues() {
        return prValues;
    }

    @JsonProperty("values")
    public void setPrValues(List<StashPr> prValues) {
        this.prValues = prValues;
    }

    @JsonProperty("size")
    public int getSize() {
        return size;
    }

    @JsonProperty("size")
    public void setSize(int size) {
        this.size = size;
    }

    public boolean getIsLastPage() {
        return isLastPage;
    }

    public int getNextPageStart() {
        return nextPageStart;
    }

}
