package stashpullrequestbuilder.stashpullrequestbuilder.stash;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Reviewer as seen in <a href="https://developer.atlassian.com/static/rest/bitbucket-server/latest/bitbucket-rest.html">doc</a>
 * <br/>
 * (look at /rest/api/1.0/projects/{projectKey}/repos/{repositorySlug}/pull-requests -> GET) doc
 *
 * @author Oleg Chernovskiy, created on 25.11.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StashPrReviewer {

    @JsonProperty
    private StashUser user;

    @JsonProperty
    private String lastReviewedCommit;

    @JsonProperty
    private String role;

    @JsonProperty
    private Boolean approved;

    @JsonProperty
    private String status;

    public StashUser getUser() {
        return user;
    }

    public void setUser(StashUser user) {
        this.user = user;
    }

    public String getLastReviewedCommit() {
        return lastReviewedCommit;
    }

    public void setLastReviewedCommit(String lastReviewedCommit) {
        this.lastReviewedCommit = lastReviewedCommit;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean isApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
