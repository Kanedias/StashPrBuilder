package stashpullrequestbuilder.stashpullrequestbuilder.stash;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Part of reviewer in our case but I assume this structure is used somewhere else too
 *
 * @see StashPrReviewer
 * @author Oleg Chernovskiy, created on 25.11.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StashUser {
    // service fields
    @JsonProperty
    private Long id;

    @JsonProperty
    private Boolean active;


    @JsonProperty
    private String name; // e.g. test_build_user

    @JsonProperty
    private String displayName; // e.g. Test Build User

    @JsonProperty
    private String emailAddress; // e.g. test.build.user@example.com

    @JsonProperty
    private String slug; // e.g. test_build_user

    @JsonProperty
    private String type; // e.g. "NORMAL"

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
