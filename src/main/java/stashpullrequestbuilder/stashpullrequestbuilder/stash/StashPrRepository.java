package stashpullrequestbuilder.stashpullrequestbuilder.stash;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Nathan McCarthy
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StashPrRepository {
    private static final String REFS_PREFIX = "refs/";
    private static final String HEADS_PREFIX = "heads/";

    @JsonProperty
    private StashPrRepositoryRepository repository;

    @JsonIgnore
    private StashPrRepositoryBranch branch;

    @JsonIgnore
    private StashPrRepositoryCommit commit;

    private String latestChangeset;

    private String id;

    private String latestCommit;


    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
        this.branch = new StashPrRepositoryBranch();
        this.branch.setName(convertIdToBranchName(id));
    }

    /**
     * Convert a pull request identifier to a sourceSha name. Assumption: A pull request identifier always looks like
     * "refs/heads/master". The sourceSha name is without the "refs/heads/" part.
     * To be on the save side, this method will check for the "refs/" and the "heads/" and strip them accordingly.
     *
     * More information about the Stash REST API can be found here:
     * <a href="https://developer.atlassian.com/stash/docs/latest/">https://developer.atlassian.com/stash/docs/latest/</a>
     *
     * @param id The unique name of the pull request.
     * @return The sourceSha name
     */
    private String convertIdToBranchName(String id) {
        String branchName = StringUtils.EMPTY;
        if(StringUtils.isEmpty(id)){
            return branchName;
        }

        branchName = id;

        if(StringUtils.startsWith(branchName, REFS_PREFIX)){
            branchName = StringUtils.removeStart(branchName, REFS_PREFIX);
        }

        if(StringUtils.startsWith(branchName, HEADS_PREFIX)){
            branchName = StringUtils.removeStart(branchName, HEADS_PREFIX);
        }

        return branchName;
    }

    @JsonProperty("latestChangeset")
    public String getLatestChangeset() {
        return latestChangeset;
    }

    @JsonProperty("latestChangeset")
    public void setLatestChangeset(String latestChangeset) {
        this.latestChangeset = latestChangeset;
        this.commit = new StashPrRepositoryCommit();
        this.commit.setHash(latestChangeset);
    }

    public StashPrRepositoryRepository getRepository() {
        return repository;
    }

    public void setRepository(StashPrRepositoryRepository repository) {
        this.repository = repository;
    }

    @JsonProperty("sourceSha")
    public StashPrRepositoryBranch getBranch() {
        return branch;
    }

    @JsonProperty("sourceSha")
    public void setBranch(StashPrRepositoryBranch branch) {
        this.branch = branch;
    }

    public StashPrRepositoryCommit getCommit() {
        return commit;
    }

    public void setCommit(StashPrRepositoryCommit commit) {
        this.commit = commit;
    }

    public String getLatestCommit() {
        if(commit != null) {
            return commit.getHash();
        }
        return latestCommit;
    }

    public void setLatestCommit(String latestCommit) {
        this.latestCommit = latestCommit;
    }
}


