package stashpullrequestbuilder.stashpullrequestbuilder.stash;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created by Nathan McCarthy
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StashPr {
    private String description; // description of this pull-request
    private Boolean locked; //

    private String title; // title of this pull-request

    private StashPrRepository toRef;

    private Boolean closed;

    private StashPrRepository fromRef;

    private String state; // open/declined/merged
    private String createdDate;
    private String updatedDate;

    private String id; // per-project id of this PR

    @JsonProperty
    private List<StashPrReviewer> reviewers;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("locked")
    public Boolean getLocked() {
        return locked;
    }

    @JsonProperty("locked")
    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public StashPrRepository getToRef() {
        return toRef;
    }

    public void setToRef(StashPrRepository toRef) {
        this.toRef = toRef;
    }

    @JsonProperty("closed")
    public Boolean getClosed() {
        return closed;
    }

    @JsonProperty("closed")
    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    public StashPrRepository getFromRef() {
        return fromRef;
    }

    public void setFromRef(StashPrRepository fromRef) {
        this.fromRef = fromRef;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("createdDate")
    public String getCreatedDate() {
        return createdDate;
    }

    @JsonProperty("createdDate")
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @JsonProperty("updatedDate")
    public String getUpdatedDate() {
        return updatedDate;
    }

    @JsonProperty("updatedDate")
    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<StashPrReviewer> getReviewers() {
        return reviewers;
    }

    public void setReviewers(List<StashPrReviewer> reviewers) {
        this.reviewers = reviewers;
    }
}
