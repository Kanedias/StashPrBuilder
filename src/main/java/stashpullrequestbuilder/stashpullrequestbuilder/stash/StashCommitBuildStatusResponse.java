package stashpullrequestbuilder.stashpullrequestbuilder.stash;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.util.List;
import java.util.Set;

/**
 * Answer on GET request to commit build status REST API
 * Looks like this:
 * <pre>
 *    {
         "size": 1,
         "limit": 25,
         "isLastPage": true,
         "values": [
           {
                "state": "SUCCESSFUL",
                "key": "REPO-MASTER",
                "name": "REPO-MASTER-42",
                "url": "https://bamboo.example.com/browse/REPO-MASTER-42",
                "description": "Changes by John Doe",
                "dateAdded": 1442554475354
           }
         ],
         "start": 0
       }
 * </pre>
 *
 * @author Oleg Chernovskiy, created on 15.07.16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class StashCommitBuildStatusResponse {

    private int size;
    private int limit;
    private boolean isLastPage;
    private int start;
    private Set<BuildStatus> values;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public boolean isLastPage() {
        return isLastPage;
    }

    public void setLastPage(boolean lastPage) {
        isLastPage = lastPage;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public Set<BuildStatus> getValues() {
        return values;
    }

    public void setValues(Set<BuildStatus> values) {
        this.values = values;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class BuildStatus {
        private String state;
        private String key;
        private String name;
        private String url;
        private String description;
        private long dateAdded;

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public long getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(long dateAdded) {
            this.dateAdded = dateAdded;
        }
    }
}
