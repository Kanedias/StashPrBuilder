package stashpullrequestbuilder.stashpullrequestbuilder.stash;

import hudson.model.Result;
import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.params.HttpConnectionParams;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Nathan McCarthy
 */
public class StashApiClient {
    private static final Logger logger = Logger.getLogger(StashApiClient.class.getName());
    private static final ObjectMapper mapper = new ObjectMapper();

    private String apiBaseUrl;
    private String bstatusBaseUrl;

    private String project;
    private String repositoryName;
    private Credentials credentials;


    public StashApiClient(String stashHost, String username, String password, String project, String repositoryName, boolean ignoreSsl) {
        this.credentials = new UsernamePasswordCredentials(username, password);
        this.project = project;
        this.repositoryName = repositoryName;
        this.apiBaseUrl = stashHost.replaceAll("/$", "") + "/rest/api/1.0/projects/";
        this.bstatusBaseUrl = stashHost.replaceAll("/$", "") + "/rest/build-status/1.0/";
        if (ignoreSsl) {
            Protocol easyhttps = new Protocol("https", (ProtocolSocketFactory) new EasySSLProtocolSocketFactory(), 443);
            Protocol.registerProtocol("https", easyhttps);
        }
    }

    public List<StashPr> getPullRequests() {
        List<StashPr> pullRequestResponseValues = new ArrayList<StashPr>();
        try {
            boolean isLastPage = false;
            int start = 0;
            while (!isLastPage) {
                String response = getResponseAsString(pullRequestsPath(start));
                StashPrListResponse parsedResponse = mapper.readValue(response, StashPrListResponse.class);
                isLastPage = parsedResponse.getIsLastPage();
                if (!isLastPage) {
                    start = parsedResponse.getNextPageStart();
                }
                pullRequestResponseValues.addAll(parsedResponse.getPrValues());
            }
            return pullRequestResponseValues;
        } catch (IOException e) {
            logger.log(Level.WARNING, "invalid pull request response.", e);
        }
        return Collections.EMPTY_LIST;
    }

    public List<StashPrComment> getPullRequestComments(String projectCode, String commentRepositoryName,
                                                       String pullRequestId) {
        try {
            boolean isLastPage = false;
            int start = 0;
            List<StashPrActivityResponse> commentResponses = new ArrayList<StashPrActivityResponse>();
            while (!isLastPage) {
                String response = getResponseAsString(
                        apiBaseUrl + projectCode + "/repos/" + commentRepositoryName + "/pull-requests/" +
                                pullRequestId + "/activities?start=" + start);
                StashPrActivityResponse resp = mapper.readValue(response, StashPrActivityResponse.class);
                isLastPage = resp.getIsLastPage();
                if (!isLastPage) {
                    start = resp.getNextPageStart();
                }
                commentResponses.add(resp);
            }
            return extractComments(commentResponses);
        } catch (Exception e) {
            logger.log(Level.WARNING, "invalid pull request response.", e);
        }
        return Collections.emptyList();
    }

    public void deletePullRequestComment(@Nonnull String pullRequestId, @Nonnull String commentId) {
        String path = pullRequestPath(pullRequestId) + "/comments/" + commentId + "?version=0";
        logger.log(Level.FINEST, "Deleting comment at " + path);
        deleteRequest(path);
    }


    public StashPrComment postPullRequestComment(@Nonnull String pullRequestId, @Nonnull String comment) {
        String path = pullRequestPath(pullRequestId) + "/comments";
        try {
            logger.log(Level.FINEST, "Posting \"" + comment + "\" to " + path);
            String response = postComment(path, comment);
            return mapper.readValue(response, StashPrComment.class);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to post Stash PR comment " + path + " " + e);
        }
        return null;
    }

    public StashPrMergeableResponse getPullRequestMergeStatus(@Nonnull String pullRequestId) {
        String path = pullRequestPath(pullRequestId) + "/merge";
        try {
            String response = getResponseAsString(path);
            return mapper.readValue( response, StashPrMergeableResponse.class);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            logger.log(Level.WARNING, "Failed to get Stash PR Invocation Status " + path + " " + e);
        }
        return null;
    }

    /**
     * Posts build status for specific commit
     * @param commitHash commit hash to link state to
     * @param params - should include "state", "key", "name", "url" and "description"
     */
    public void postCommitBuilt(@Nonnull String commitHash, @Nonnull List<NameValuePair> params) {
        String path = bstatusBaseUrl + "commits/" + commitHash;
        logger.log(Level.INFO, "PR-POST-REQUEST: " + path + " with: " + params);

        ObjectNode node = mapper.getNodeFactory().objectNode();
        for (NameValuePair param : params) {
            node.put(param.getName(), param.getValue());
        }

        sendObject("POST", path, node);
    }

    public StashCommitBuildStatusResponse getCommitBuildStatus(@Nonnull String commitHash) throws IOException {
        String response = getResponseAsString(bstatusBaseUrl + "commits/" + commitHash);
        return mapper.readValue(response, StashCommitBuildStatusResponse.class);
    }

    private HttpClient getHttpClient() {
        HttpClient client = new HttpClient();
        return client;
    }

    private String getResponseAsString(@Nonnull String path) {
        logger.log(Level.FINEST, "PR-GET-REQUEST:" + path);
        HttpClient client = getHttpClient();
        client.getState().setCredentials(AuthScope.ANY, credentials);
        GetMethod httpget = new GetMethod(path);
        client.getParams().setAuthenticationPreemptive(true);
        String response = null;
        int responseCode;
        try {
            responseCode = client.executeMethod(httpget);
            InputStream responseBodyAsStream = httpget.getResponseBodyAsStream();
            StringWriter stringWriter = new StringWriter();
            IOUtils.copy(responseBodyAsStream, stringWriter, "UTF-8");
            response = stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to process PR get request; " + path, e);
        }
        logger.log(Level.FINEST, "PR-GET-RESPONSE:" + response);
        if (!validResponseCode(responseCode)) {
            logger.log(Level.SEVERE, "Failing to get response from Stash PR GET " + path);
            throw new RuntimeException("Didn't get a 200 response from Stash PR GET! Response; '" +
                    HttpStatus.getStatusText(responseCode) + "' with message; " + response);
        }
        return response;
    }

    public void deleteRequest(@Nonnull String path) {
        HttpClient client = getHttpClient();
        client.getState().setCredentials(AuthScope.ANY, credentials);
        DeleteMethod httppost = new DeleteMethod(path);
        client.getParams().setAuthenticationPreemptive(true);
        int res = -1;
        try {
            res = client.executeMethod(httppost);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to delete Stash PR comment " + path);
        }
        logger.log(Level.FINE, "Delete comment {" + path + "} returned result code; " + res);
    }

    private String postComment(@Nonnull String path, @Nonnull String comment) throws UnsupportedEncodingException {
        logger.log(Level.FINEST, "PR-POST-REQUEST:" + path + " with: " + comment);
        HttpClient client = getHttpClient();
        client.getState().setCredentials(AuthScope.ANY, credentials);
        PostMethod httppost = new PostMethod(path);

        ObjectNode node = mapper.getNodeFactory().objectNode();
        node.put("text", comment);

        StringRequestEntity requestEntity = null;
        try {
            requestEntity = new StringRequestEntity(
                    mapper.writeValueAsString(node),
                    "application/json",
                    "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        httppost.setRequestEntity(requestEntity);
        client.getParams().setAuthenticationPreemptive(true);
        String response = "";
        int responseCode;
        try {
            responseCode = client.executeMethod(httppost);
            InputStream responseBodyAsStream = httppost.getResponseBodyAsStream();
            StringWriter stringWriter = new StringWriter();
            IOUtils.copy(responseBodyAsStream, stringWriter, "UTF-8");
            response = stringWriter.toString();
            logger.log(Level.FINEST, "API Request Response: " + response);
        }  catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to process PR get request; " + path, e);
        }
        logger.log(Level.FINEST, "PR-POST-RESPONSE:" + response);
        if (!validResponseCode(responseCode)) {
            logger.log(Level.SEVERE, "Failing to get response from Stash PR POST" + path);
            throw new RuntimeException("Didn't get a 200 response from Stash PR POST! Response; '" +
                    HttpStatus.getStatusText(responseCode) + "' with message; " + response);
        }
        return response;
    }

    private boolean validResponseCode(int responseCode) {
        return responseCode == HttpStatus.SC_OK ||
                responseCode == HttpStatus.SC_ACCEPTED ||
                responseCode == HttpStatus.SC_CREATED ||
                responseCode == HttpStatus.SC_NO_CONTENT ||
                responseCode == HttpStatus.SC_RESET_CONTENT ;
    }

    private List<StashPrComment> extractComments(List<StashPrActivityResponse> responses) {
        List<StashPrComment> comments = new ArrayList<StashPrComment>();
        for (StashPrActivityResponse parsedResponse: responses) {
            for (StashPrActivity a : parsedResponse.getPrValues()) {
                if (a != null && a.getComment() != null) comments.add(a.getComment());
            }
        }
        return comments;
    }

    private String pullRequestsPath() {
        return apiBaseUrl + this.project + "/repos/" + this.repositoryName + "/pull-requests/";
    }

    private String pullRequestPath(String pullRequestId) {
        return pullRequestsPath() + pullRequestId;
    }

    private String pullRequestParticipantPath(String prId, String ptcpSlug) {
        return pullRequestPath(prId) + "/participants/" + ptcpSlug;
    }

    private String pullRequestsPath(int start) {
        String basePath = pullRequestsPath();
        return basePath.substring(0, basePath.length() - 1) + "?start=" + start;
    }

    public void postApproveStatus(@Nonnull StashPr pullRequest, @Nonnull StashPrReviewer reviewer, @Nullable Result result) {
        String ptcpPath = pullRequestParticipantPath(pullRequest.getId(), reviewer.getUser().getSlug());
        String status = result == null ? "UNAPPROVED" : result == Result.SUCCESS ? "APPROVED" : "NEEDS_WORK";
        String username = reviewer.getUser().getName();

        logger.log(Level.FINEST, "PR-POST-REQUEST:" + ptcpPath + " with: " + status);

        ObjectNode node = mapper.getNodeFactory().objectNode();
        ObjectNode user = mapper.getNodeFactory().objectNode();
        user.put("name", username);
        node.put("status", status);
        node.put("approved", true);
        node.put("user", user);

        sendObject("PUT", ptcpPath, node);
    }

    private void sendObject(@Nonnull String reqType, @Nonnull String path, @Nonnull ObjectNode node) {
        HttpClient client = getHttpClient();
        client.getState().setCredentials(AuthScope.ANY, credentials);

        try {
            StringRequestEntity requestEntity = new StringRequestEntity(
                    mapper.writeValueAsString(node),
                    "application/json",
                    "UTF-8");

            EntityEnclosingMethod reqMethod;
            switch (reqType) {
                case "PUT":
                    reqMethod = new PutMethod(path);
                    break;
                case "POST":
                default:
                    reqMethod = new PostMethod(path);
                    break;
            }

            reqMethod.setRequestEntity(requestEntity);
            client.getParams().setAuthenticationPreemptive(true);
            int responseCode = client.executeMethod(reqMethod);
            if (!validResponseCode(responseCode)) {
                logger.log(Level.SEVERE, "Failing to get response from Stash commit POST: " + path);
                throw new RuntimeException("Didn't get a 200 response from Stash commit POST! Response: '" +
                        HttpStatus.getStatusText(responseCode) + "'");
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Network exception while posting build status", e);
            throw new RuntimeException("Failed to process PR post response request; " + path, e);
        }
    }

    private static class EasySSLProtocolSocketFactory extends org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory {
        private static final Log LOG = LogFactory.getLog(EasySSLProtocolSocketFactory.class);
        private SSLContext sslcontext = null;

        private static SSLContext createEasySSLContext() {
            try {
                TrustManager[] trustAllCerts = new TrustManager[]{
                        new X509TrustManager(){
                            public X509Certificate[] getAcceptedIssuers(){ return null; }
                            public void checkClientTrusted(X509Certificate[] certs, String authType) {}
                            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
                        }
                };

                SSLContext context = SSLContext.getInstance("SSL");
                context.init(
                        null,
                        trustAllCerts,
                        null);
                return context;
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
                throw new HttpClientError(e.toString());
            }
        }

        private SSLContext getSSLContext() {
            if (this.sslcontext == null) {
                this.sslcontext = createEasySSLContext();
            }
            return this.sslcontext;
        }

        public Socket createSocket(String host, int port, InetAddress clientHost, int clientPort) throws IOException, UnknownHostException {
            return this.getSSLContext().getSocketFactory().createSocket(host, port, clientHost, clientPort);
        }

        public Socket createSocket(String host, int port, InetAddress localAddress, int localPort, HttpConnectionParams params) throws IOException, UnknownHostException, ConnectTimeoutException {
            if(params == null) {
                throw new IllegalArgumentException("Parameters may not be null");
            } else {
                int timeout = params.getConnectionTimeout();
                SSLSocketFactory socketfactory = this.getSSLContext().getSocketFactory();
                if(timeout == 0) {
                    return socketfactory.createSocket(host, port, localAddress, localPort);
                } else {
                    Socket socket = socketfactory.createSocket();
                    InetSocketAddress localaddr = new InetSocketAddress(localAddress, localPort);
                    InetSocketAddress remoteaddr = new InetSocketAddress(host, port);
                    socket.bind(localaddr);
                    socket.connect(remoteaddr, timeout);
                    return socket;
                }
            }
        }

        public Socket createSocket(String host, int port) throws IOException {
            return this.getSSLContext().getSocketFactory().createSocket(host, port);
        }

        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException {
            return this.getSSLContext().getSocketFactory().createSocket(socket, host, port, autoClose);
        }
    }
}

