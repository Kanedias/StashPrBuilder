package stashpullrequestbuilder.stashpullrequestbuilder.stash;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test for checking the logic inside the StashPrRepository class.
 * Since most of this class consists of getters/setters only one method gets tested.
 *
 * @author bart
 */
public class StashPrListResponseValueRepositoryTest {

    StashPrRepository stashPrRepository;

    @Before
    public void setUp() throws Exception {
        stashPrRepository =
                new StashPrRepository();
    }

    @Test
    public void testSetIdWithNullValue() throws Exception {
        // Test on null values
        stashPrRepository.setId(null);
        String branchName = stashPrRepository.getBranch().getName();
        assertEquals("", branchName);
    }

    @Test
    public void testSetIdWithEmptyValue() throws Exception {
        // Test on empty String
        stashPrRepository.setId("");
        String branchName = stashPrRepository.getBranch().getName();
        assertEquals("", branchName);
    }

    @Test
    public void testSetIdWithSlashInBranchName() throws Exception {

        // Test if the sourceSha name get extracted the right way. Allowing for '/' in the sourceSha name
        stashPrRepository.setId("refs/heads/release/1.0.0");
        String branchNameWithSlash= stashPrRepository.getBranch().getName();
        assertEquals("release/1.0.0", branchNameWithSlash);
    }

    @Test
    public void testSetIdWithNormalBranchName() throws Exception {

        // Normal case, a sourceSha name without '/' characters
        stashPrRepository.setId("refs/heads/master");
        String branchNameWithoutSlash = stashPrRepository.getBranch().getName();
        assertEquals("master", branchNameWithoutSlash);

    }

    @Test
    public void testSetIdWithUnexpectedBranchName() throws Exception {

        // Normal case, but now with a weird pull request identifier
        stashPrRepository.setId("refs/weird/master");
        String branchNameWithoutSlash = stashPrRepository.getBranch().getName();
        assertEquals("weird/master", branchNameWithoutSlash);

    }
}